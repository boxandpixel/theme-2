# Brian Gunzenhauser

This repository includes example Wordpress theme files. 

## Getting Started

All theme files can be reviewed by navigating to /wp-content/themes/ineotech in this repository.

## Built With

* [FoundationPress](https://github.com/olefredrik/foundationpress) - This site was built on top of the FoundationPress responsive starter framework. 
* [Flipster](https://github.com/drien/jquery-flipster) - A jQuery 3D transform plugin

## Authors

* **Brian Gunzenhauser**
