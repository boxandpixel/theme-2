// Home Page Scrolling


// Initialize Variables
var bodyEl = $('body');
var htmlEl = $('html');
var touchActive;
if (htmlEl.hasClass('touchevents')) {
	touchActive = 'active';
}
var is_chrome;
var dataPage = $('body').attr('data-page');
var jsScrollingContent = $('.js-scrolling-content');

var handCursorX = 0;
var handCursorY = 0;
var windowWidth;
var windowHeight;
var halfWindowWidth;
var halfWindowHeight;

// Page Element Variables
var jsReturnToTop 		= $('.js-return-to-top');
var pageOutLoad 		= $('.page-out-load');

// Mobile Menu Variables
var jsMobNavBtn = $('.js-mob-nav-btn');
var jsMobNav 	= $('.js-mob-nav');

// Home Scene Variables
var jsHomeScene 		= $('.home-scene');
var jsHomeFadeout		= $('.js-home-fadeout');
var jsHomeLines 		= $('.home-lines');
var homeHeaderScrollGap;
var homeSceneFadeBuffer;
var homeSceneFadeStart;

// Scrolling Variables
var jsScrollDown = $('.js-scroll-down');
var jsScrollWrap = $('.js-scroll-wrap');
var scrollYPos;
var homeSceneFadeOut;


var queue = new createjs.LoadQueue(true);
var scrollcontainer = document.getElementById('ps-scroll-container');
Ps.initialize(scrollcontainer);

// Scrolling effect on page load
$(document).ready(function() {

	//loadSite();
	detectBrowser();
	sizeHandler();

	if (dataPage == 'home') {
		if ( touchActive == 'active' ) {
			mobHomeScrollInterval();
		} else {
			mouseCursorFollowerInterval();
			homeScrollInterval();
		}
	}

	if ( touchActive == 'active' ) {
		mobNavScrollInterval();
	}

	// BUTTONS

	$('a').click(function(e) {
		var thisTarget = $(this).attr('target');
		var thisHref = $(this).attr('href');

		if (thisTarget!="_blank" && thisHref.indexOf('mailto') == -1 && thisHref.indexOf('tel:') == -1) {
			e.preventDefault();
			if (thisHref!='#') {
				pageLinkTransition(thisHref);
			}
		}
	}); 

	jsMobNavBtn.on('click', function(e){
		if (jsMobNav.attr('data-active') == "off") {
			dataActiveOn(jsMobNav);
			dataActiveOn($(this));
		} else {
			dataActiveOff(jsMobNav);
			dataActiveOff($(this));
		}
	});

	jsScrollDown.on('click', function(e){
		var scrollToElement = '.' + $(this).attr('data-scroll-to-element');
		var scrollSpeed = parseInt($(this).attr('data-scroll-speed'));
		var scrollToVal = $(scrollToElement).offset().top - (windowHeight * 0.3);
		//console.log(scrollToVal);

	    if ( touchActive == 'active' ) {
			setTimeout(function(){
				jsScrollingContent.animate({
			        scrollTop: scrollToVal
			    }, scrollSpeed, 'easeInOutCirc');
		    }, 10);
		} else {
			jsScrollingContent.animate({
		        scrollTop: scrollToVal
		    }, scrollSpeed, 'easeInOutCirc');
		}
	});

});


function pageLinkTransition(hr) {
	if (is_chrome === true) {
    	pageOutLoad.show();

		setTimeout(function(){
			dataActiveOn(pageOutLoad);
			setTimeout(function(){
				location.href=hr;
			}, 200);
		}, 10);

    } else {
    	location.href=hr;
    }
	
	
}
function showElementWithTransition(e) {
	e.show();
	setTimeout(function(){
	   	e.addClass('active');
	}, 20);
}

function hideElementWithTransition(e) {
	e.removeClass('active');
	setTimeout(function(){
	   	e.hide();
	}, 1000);
}

function sizeHandler() {
	windowWidth = $(window).innerWidth();
	windowHeight = $(window).height();
	halfWindowWidth = windowWidth / 2;
	halfWindowHeight = windowHeight / 2;
	homeHeaderScrollGap = $('.js-home-header-gap').height();

	var innerWindowHeight = $(window).innerHeight();
	$('.js-full-height').each(function() {
      	$(this).css({ 
		    'height' : innerWindowHeight
	    });
    });

    if ( touchActive == 'active' ) {
		arrowTop = innerWindowHeight - 70;
		$('.js-scroll-wrap').css({ 
		    'top' : arrowTop
	    });
	}

	// Lower blends sooner
	homeSceneFadeBuffer = homeHeaderScrollGap / 2; // THE DISTANCE COVERED BY THE FADE OUT

	// Lower blends slower
	homeSceneFadeStart = homeHeaderScrollGap - (homeSceneFadeBuffer*2); // THE NUMBER NEEDED TO CALCULATE THE FADE
}

$(window).resize(function() {

	sizeHandler();

}); // End of resize

$(window).scroll(function() {
});  // End of scroll



function mouseCursorFollowerInterval() {
	
	htmlEl.mousemove(function(e) {
	  	handCursorX = e.pageX;
    	handCursorY = e.pageY;
    });
}

// Scroll scene on home
function homeScrollInterval() {
	scrollYPos = jsScrollingContent.scrollTop();

	homeSceneFadeOut = 1 + ( (homeSceneFadeStart - scrollYPos) / homeSceneFadeBuffer );

	TweenMax.to(jsHomeFadeout, 
		0.3, {
			opacity: homeSceneFadeOut
		}
	);

    if (scrollYPos >= 100) {
    	dataActiveOff(jsScrollWrap);
    } else {
    	dataActiveOn(jsScrollWrap);
    }

    // Fade Logo


    if (scrollYPos >= 100) {
    	$("#nav-logo").addClass("logo-fade-in");
    } else {
    	$("#nav-logo").removeClass("logo-fade-in");
    } 

    if (scrollYPos >= 100) {
    	$(".title-bar-home").fadeIn();
    } else {
    	$(".title-bar-home").fadeOut();
    }     

	setTimeout(function(){
		homeScrollInterval();
    }, 10);
}

function mobHomeScrollInterval() {
	scrollYPos = jsScrollingContent.scrollTop();
	if (scrollYPos >= 30) {
    	dataActiveOff(jsScrollWrap);
    } else {
    	dataActiveOn(jsScrollWrap);
    }
    setTimeout(function(){
		mobHomeScrollInterval();
    }, 10);
}

function loadSite() {

	preload = new createjs.LoadQueue();
	preload.on("complete", siteLoaded); // ON ALL LOADED, RUN FUNCTION siteLoaded
	preload.on("progress", loadProgress); // ON ALL LOADED, RUN FUNCTION siteLoaded

	if (dataPage == 'home') {
		//preload.loadFile("/img/home-scene/sky.jpg");
		//preload.loadFile("/img/home-scene/tinified/mid-ground-opt.png");
		//preload.loadFile("/img/home-scene/tinified/mid-mountains.png");
		//preload.loadFile("/img/home-scene/tinified/mountains.png");
	}
}

function loadProgress() {
	if (preload.progress == 1) {

		setTimeout(function(){
			bodyEl.attr('data-loaded', 'true');
			if (dataPage == "home") {
				setTimeout(function(){
			   		fogRepeat();
				}, 17000);
			}
			setTimeout(function(){
		   		$('.preloader-wrap').remove();
			}, 1900);
		}, 400);
	}
}

function siteLoaded() {
	setTimeout(function(){
		
   		$('.preloader-wrap').attr('data-loaded', 'true');
		setTimeout(function(){
	   		$('.preloader-wrap').remove();
		}, 1800);
	}, 600);
}

function detectBrowser() {

	is_chrome = navigator.userAgent.indexOf('Chrome') > -1;
    
    if (is_chrome === true) {
    	$('.fog').addClass('movement');
    }

}

// UTILITY FUNCTIONS

function dataActiveOff(e) {
	e.attr('data-active', 'off');
}
function dataActiveOn(e) {
	e.attr('data-active', 'on');
}

// HTML5 Canvas
