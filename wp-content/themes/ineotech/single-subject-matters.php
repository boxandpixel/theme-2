<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php get_template_part( 'template-parts/featured-image' ); ?>

<div id="single-post" role="main" class="single-subject-matters">

<?php do_action( 'foundationpress_before_content' ); ?>

     	
    <div class="secondary-nav">
      <div id="service-nav-title">
        <span class="overview-page">Knowledge Resources</span>
        <span class="overview-title">Subject Matters</span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'knowledge-resources-menu' ) ); ?>

    </div> <!-- .secondary-nav --> 
 
   

    <div id="main">

    <div class="row">
    	<div class="small-12 large-8 columns">
<?php while ( have_posts() ) : the_post(); ?>    		
    		<article id="post-<?php the_ID(); ?>" class="subject-main">

          <h1 class="article-title"><?php the_title(); ?></h1>
          <div class="row collapse">
            <div class="small-12 medium-2 columns">

<?php
            $author_image = get_field("author_image");
            $author_image_url = $author_image['url'];
            $author_image_alt = $author_image['alt'];
?>
              <img src="<?php echo $author_image_url; ?>" alt="<?php echo $author_image_alt; ?>">
            </div> <!-- .small-2 -->
            <div class="small-12 medium-10 columns">
              
              <div class="author-info">
                <span class="author-name"><?php the_field("author_name"); ?></span>
                <span class="author-title"><?php the_field("author_title"); ?></span>
                <span class="author-email"><?php the_field("author_email"); ?></span>
              </div> <!-- .author-info -->
            </div> <!-- .columns -->          
          </div> <!-- .row -->   
                 
          <div class="visual-editor">
            <?php the_content('', false); ?>
<?php
        if(have_rows("knowledge_resources_content")):
          while(have_rows("knowledge_resources_content")): the_row();

            if(get_row_layout() == "visual_editor"):

              the_sub_field("visual_editor");

            elseif(get_row_layout() == "image"):

              the_sub_field("image");

              $image = get_sub_field("image");
              $image_url = $image['url'];
              $image_alt = $image['alt'];
?>
              <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php 
            elseif(get_row_layout() == "accordion"):

              if(have_rows("accordion")):
                while(have_rows("accordion")): the_row();
?>
                  <div class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<?php
                    while(have_rows("accordion")): the_row();
?>
                    <div class="accordion-item" data-accordion-item>
                      <a href="#" class="accordion-title"><?php the_sub_field("accordion_title"); ?></a>
                      <div class="accordion-content" data-tab-content>
                      <?php the_sub_field("accordion_content"); ?>
                      </div> <!-- .accordion-content -->
                    </div> <!-- .accordion-item -->
<?php
                    endwhile; // have_rows('service_accordion')
?>
                  </div> <!-- .accordion -->
<?php                      
                endwhile;
              endif;

            elseif(get_row_layout() == "pdf"):
              $pdf = get_sub_field("pdf");
              $pdf_url = $pdf['url'];
?>
            <a href="<?php echo $pdf_url; ?>" class="button-white">Download PDF</a>                  

<?php
            endif; // get_row_layout
          endwhile;
        endif;

?>                
          </div> <!-- .visual-editor -->
        </article>
<?php endwhile;?>            
    	</div> <!-- .columns -->
    	<div class="small-12 large-4 columns">
                <div class="overview-sidebar">

                  <h2>View all articles:</h2>
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 5,
        //'offset' => 1,
        'post_type' => 'subject-matters', 
        'orderby' => 'date',
        'order' => 'desc',
        'post__not_in' => array($post->ID)
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
              <div class="sidebar-article">

                <h1><?php the_title(); ?></h1>
                <div class="sidebar-article-author">
                  <span class="author-name"><?php the_field("author_name"); ?></a>
                  <span class="author-title"><?php the_field("author_title"); ?></a>
                </div> <!-- .sidebar-article-author -->
                <div class="sidebar-excerpt">
                  <?php the_field("article_intro"); ?>
                  <p><a href="<?php the_permalink(); ?>" class="more-link">Read More</a></p>                             
                </div> <!-- .sidebar-excerpt -->
              </div> <!-- .sidebar-article -->
<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>                       
                </div> <!-- .overview-sidebar -->
    	</div> <!-- .columns -->
	</div> <!-- .row -->    	 

	</div> <!-- #main -->


<?php do_action( 'foundationpress_after_content' ); ?>

</div> <!-- #single-post -->
<?php get_footer();
