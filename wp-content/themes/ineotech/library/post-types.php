<?php

// Register Webinars
function register_webinars() {
	register_post_type('webinars', array(
		'label' => 'Webinars',
		'menu_icon' => 'dashicons-admin-site',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions'),
		'labels' => array (
			'name' => 'Webinars',
			'singular_name' => 'Webinar',
			'menu_name' => 'Webinars',
			'add_new' => 'Add Webinar',
			'add_new_item' => 'Add New Webinar',
			'edit' => 'Edit',
			'edit_item' => 'Edit Webinar',
			'new_item' => 'New Webinar',
			'view' => 'View',
			'view_item' => 'View Webinar',
			'search_items' => 'Search Webinars',
			'not_found' => 'No Webinars Found',
			'not_found_in_trash' => 'No Webinars Found in Trash',
			'parent' => 'Parent Webinar'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_webinars');

function register_speaking_engagements() {
	register_post_type('speaking-engagements', array(
		'label' => 'Speaking Engagements',
		'menu_icon' => 'dashicons-megaphone',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions'),
		'labels' => array (
			'name' => 'Speaking Engagements',
			'singular_name' => 'Speaking Engagement',
			'menu_name' => 'Speaking Engagements',
			'add_new' => 'Add Speaking Engagement',
			'add_new_item' => 'Add New Speaking Engagement',
			'edit' => 'Edit',
			'edit_item' => 'Edit Speaking Engagement',
			'new_item' => 'New Speaking Engagement',
			'view' => 'View',
			'view_item' => 'View Speaking Engagement',
			'search_items' => 'Search Speaking Engagements',
			'not_found' => 'No Speaking Engagements Found',
			'not_found_in_trash' => 'No Speaking Engagements Found in Trash',
			'parent' => 'Parent Speaking Engagement'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_speaking_engagements');

// Register Mobility Solutions
function register_mobility_solutions() {
	register_post_type('mobility-solutions', array(
		'label' => 'Mobility Solutions',
		'menu_icon' => 'dashicons-format-aside',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions'),
		'labels' => array (
			'name' => 'Mobility Solutions',
			'singular_name' => 'Mobility Solution',
			'menu_name' => 'Mobility Solutions',
			'add_new' => 'Add Mobility Solution',
			'add_new_item' => 'Add New Mobility Solution',
			'edit' => 'Edit',
			'edit_item' => 'Edit Mobility Solution',
			'new_item' => 'New Mobility Solution',
			'view' => 'View',
			'view_item' => 'View Mobility Solution',
			'search_items' => 'Search Mobility Solutions',
			'not_found' => 'No Mobility Solutions Found',
			'not_found_in_trash' => 'No Mobility Solutions Found in Trash',
			'parent' => 'Parent Mobility Solution'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_mobility_solutions');

// Register Tax Updates
function register_tax_updates() {
	register_post_type('tax-updates', array(
		'label' => 'Tax Updates',
		'menu_icon' => 'dashicons-format-aside',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions', 'editor'),
		'labels' => array (
			'name' => 'Tax Updates',
			'singular_name' => 'Tax Update',
			'menu_name' => 'Tax Updates',
			'add_new' => 'Add Tax Update',
			'add_new_item' => 'Add New Tax Update',
			'edit' => 'Edit',
			'edit_item' => 'Edit Tax Updates',
			'new_item' => 'New Tax Updates',
			'view' => 'View',
			'view_item' => 'View Tax Updates',
			'search_items' => 'Search Tax Updates',
			'not_found' => 'No Tax Updates Found',
			'not_found_in_trash' => 'No Tax Updates Found in Trash',
			'parent' => 'Parent Tax Update'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_tax_updates');


// Register Subject Matters
function register_subject_matters() {
	register_post_type('subject-matters', array(
		'label' => 'Subject Matters',
		'menu_icon' => 'dashicons-format-aside',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions', 'editor'),
		'labels' => array (
			'name' => 'Subject Matters',
			'singular_name' => 'Subject Matter',
			'menu_name' => 'Subject Matters',
			'add_new' => 'Add Subject Matter',
			'add_new_item' => 'Add New Subject Matter',
			'edit' => 'Edit',
			'edit_item' => 'Edit Subject Matters',
			'new_item' => 'New Subject Matters',
			'view' => 'View',
			'view_item' => 'View Subject Matters',
			'search_items' => 'Search Subject Matters',
			'not_found' => 'No Subject Matters Found',
			'not_found_in_trash' => 'No Subject Matters Found in Trash',
			'parent' => 'Parent Subject Matter'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_subject_matters');


// Register News
function register_news() {
	register_post_type('news', array(
		'label' => 'News',
		'menu_icon' => 'dashicons-format-aside',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => false,
		'supports' => array('title','revisions'),
		'labels' => array (
			'name' => 'News',
			'singular_name' => 'News',
			'menu_name' => 'News',
			'add_new' => 'Add News',
			'add_new_item' => 'Add New News',
			'edit' => 'Edit',
			'edit_item' => 'Edit News',
			'new_item' => 'New News',
			'view' => 'View',
			'view_item' => 'View News',
			'search_items' => 'Search News',
			'not_found' => 'No News Found',
			'not_found_in_trash' => 'No News Found in Trash',
			'parent' => 'Parent News'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'register_news');

?>