<?php 
// Insert style select on WYSIWYG editor
if ( ! function_exists( 'my_mce_buttons_2' ) ) :
	function my_mce_buttons_2( $buttons ) {
		array_unshift( $buttons, 'styleselect' );
		array_unshift( $buttons, 'fontsizeselect');
		return $buttons;
	}

	add_filter('mce_buttons_2', 'my_mce_buttons_2');
endif;

// Define styles for style select in WYSIWYG editor
if ( ! function_exists( 'my_mce_before_init_insert_formats' ) ) :

	function my_mce_before_init_insert_formats( $init_array ) {  
		
		// Create array for list of styles
		$style_formats = array(  

			// Define each style

			array(  
				'title' => 'Footer Contact Heading',  
				'inline' => 'span',  
				'classes' => 'footer-contact-heading',
				'wrapper' => false,
				
			),

			array(  
				'title' => 'Footer Heading',  
				'inline' => 'span',  
				'classes' => 'footer-heading',
				'wrapper' => false,
				
			),			
			array(
				'title' => 'Footer Address Wrapper',
				'block' => 'div',
				'classes' => 'footer-address-wrapper',
				'wrapper' => true,
			),
			array(
				'title' => 'Footer Address Line',
				'inline' => 'span',
				'classes' => 'footer-address-line',
				'wrapper' => false,
			),
			array(
				'title' => 'Overview Sidebar Name/Title',
				'inline' => 'span',
				'classes' => 'overview-sidebar-name-title',
				'wrapper' => false,
			),
			array(
				'title' => 'Article Intro',
				'inline' => 'span',
				'classes' => 'article-intro',
				'wrapper' => false,
			),
			array(
				'title' => 'Blue',
				'inline' => 'span',
				'classes' => 'blue',
				'wrapper' => false,
			),
			array(
				'title' => 'Tax Guide Description',
				'inline' => 'span',
				'classes' => 'tax-guide-description',
				'wrapper' => false,
			),
			array(
				'title' => 'Sidebar Phone Number',
				'inline' => 'span',
				'classes' => 'sidebar-phone-number',
				'wrapper' => false,
			),
			array(
				'title' => 'Registration Closed',
				'inline' => 'span',
				'classes' => 'registration-closed',
				'wrapper' => false,
			),
			array(
				'title' => 'Intro Heading',
				'inline' => 'span',
				'classes' => 'intro-heading',
				'wrapper' => false,
			),
			array(
				'title' => 'Contact Us Name',
				'inline' => 'span',
				'classes' => 'contact-us-name',
				'wrapper' => false,
			),
			array(
				'title' => 'Contact Us Number',
				'inline' => 'span',
				'classes' => 'contact-us-number',
				'wrapper' => false,
			),
			array(
				'title' => 'Museo Font',
				'inline' => 'span',
				'classes' => 'museo-font',
				'wrapper' => false,
			),
			array(
				'title' => 'Asterisk',
				'inline' => 'span',
				'classes' => 'asterisk',
				'wrapper' => false,
			),
			array(
				'title' => 'Tax Q&A Hours',
				'inline' => 'span',
				'classes' => 'tax-qa-hours',
				'wrapper' => false,
			),
			array(
				'title' => 'Tax Q&A Hours Answered',
				'inline' => 'span',
				'classes' => 'tax-qa-hours-answered',
				'wrapper' => false,
			),
			array(
				'title' => 'Tax Q&A Price',
				'inline' => 'span',
				'classes' => 'tax-qa-price',
				'wrapper' => false,
			),
			array(
				'title' => 'Font Weight Light',
				'inline' => 'span',
				'classes' => 'font-weight-light',
				'wrapper' => false,
			)						
		);  
		
		$init_array['style_formats'] = json_encode( $style_formats );  
		
		return $init_array;  
	  
	} 

	add_filter( 'tiny_mce_before_init', 'my_mce_before_init_insert_formats' );  
endif;

// Add footer custom fields
if( function_exists('acf_add_options_page') ) {

	acf_add_options_page(array(
		'page_title' 	=> 'Footer Content',
		'menu_title'	=> 'Footer Content',
		'menu_slug' 	=> 'footer-content',
		'capability'	=> 'edit_posts',
		'parent_slug'	=> 'themes.php',
		'redirect'		=> false
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Header Content',
		'menu_title'	=> 'Header Content',
		'menu_slug' 	=> 'header-content',
		'capability'	=> 'edit_posts',
		'parent_slug'	=> 'themes.php',
		'redirect'		=> false
	));	

}

// Allow SVG uploads in Media Library
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// Enable custom style sheet for TinyMCE (wysiwyg)
if ( ! function_exists( 'my_theme_add_editor_styles' ) ) :
function my_theme_add_editor_styles() {
    add_editor_style( 'custom-editor-style.css' );
}
add_action( 'init', 'my_theme_add_editor_styles' );
endif;

// Register subnav menus

function register_my_menus() {
  register_nav_menus(
    array(
      //'header-menu' => __( 'Header Menu' ),
      'financial-solutions-menu' => __( 'Financial Solutions Menu' ),
      'tax-services-menu' => __( 'Tax Services Menu' ),
      'mobility-software-menu' => __( 'Mobility Software Menu' ),
      'knowledge-resources-menu' => __( 'Knowledge Resources Menu'),
    )
  );
}
add_action( 'init', 'register_my_menus' );

// Add custom size for sponsors on Webinars > Sponsors

add_image_size( 'sponsor-image', 225, 115);

?>