<?php

// Register Custom Post Type
function alphaindex_register_employee() {
	register_post_type('employee', array(
		'label' => 'Employees',
		'menu_icon' => 'dashicons-groups',
		'description' => '',
		'public' => true,
		'show_ui' => true,
		'show_in_menu' => true,
		'capability_type' => 'post',
		'map_meta_cap' => true,
		'hierarchical' => false,
		'query_var' => true,
		'has_archive' => true,
		'supports' => array('title','revisions'),
		'labels' => array (
			'name' => 'Employees',
			'singular_name' => 'Employee',
			'menu_name' => 'Employees',
			'add_new' => 'Add Employee',
			'add_new_item' => 'Add New Employee',
			'edit' => 'Edit',
			'edit_item' => 'Edit Employee',
			'new_item' => 'New Employee',
			'view' => 'View',
			'view_item' => 'View Employee',
			'search_items' => 'Search Employees',
			'not_found' => 'No Employees Found',
			'not_found_in_trash' => 'No Employees Found in Trash',
			'parent' => 'Parent Employee'
		)
	) );

	flush_rewrite_rules( false );
}
add_action('init', 'alphaindex_register_employee');


// Register Alpha Taxonomies

function alphaindex_alpha_tax() {
	register_taxonomy( 'alpha',array (
		0 => 'employee',
	),
	array( 'hierarchical' => false,
		'label' => 'Alpha',
		'show_ui' => true,
		'query_var' => true,
		'show_admin_column' => false,
	) );
}
add_action('init', 'alphaindex_alpha_tax');


// Save posts to correct taxonomy
/*
function alphaindex_save_alpha( $post_id ) {
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
	return;
	//only run for songs
	$slug = 'employee';
	$letter = '';
	// If this isn't an 'employee' post, don't update it.
	if ( isset( $_POST['post_type'] ) && ( $slug != $_POST['post_type'] ) )
	return;
	// Check permissions
	if ( !current_user_can( 'edit_post', $post_id ) )
	return;
	// OK, we're authenticated: we need to find and save the data

	// May need to create a new field called "order" to dictate order. Where to put this? Maybe on the about page after an employee is entered?
	$taxonomy = 'alpha';
	if ( isset( $_POST['post_type'] ) ) {
		// Get the title of the post
		$title = strtolower( $_POST['post_title'] );
		
		// The next few lines remove A, An, or The from the start of the title
		//$splitTitle = explode(" ", $title);
		//$title = implode(" ", $splitTitle);
		
		// Get the first letter of the title
		$letter = substr( $title, 0, 1 );
		
		// Set to 0-9 if it's a number
		if ( is_numeric( $letter ) ) {
			$letter = '0-9';
		}
	}
	//set term as first letter of post title, lower case
	wp_set_post_terms( $post_id, $letter, $taxonomy );
}
add_action( 'save_post', 'alphaindex_save_alpha' );
*/
?>