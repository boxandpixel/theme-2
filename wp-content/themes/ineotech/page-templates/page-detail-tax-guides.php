<?php
/**
 * Template Name: Tax Services > Tax Guides
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior service-detail detail-tax">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'tax-services-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div id="main">


      <div class="row">
        <div class="small-11 small-centered columns">

          <div class="row">
              <div class="small-12 large-8 columns">
                <div class="detail-main">
                  <h1><?php the_field("service_page_title"); ?></h1>

<?php
                  if(have_rows("service_page_content")):
                    while(have_rows("service_page_content")): the_row();
?>

<?php
                    if(get_row_layout()=="visual_editor"):
?>
                      <?php the_sub_field("service_visual_editor"); ?>
<?php
                    elseif(get_row_layout()=="image"):

                      $image = get_sub_field("service_image");
                      $image_url = $image['url'];
                      $image_alt = $image['alt'];
?>
                      <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php
                    elseif(get_row_layout()=="text"):
?>
                      <?php the_sub_field("service_text"); ?>

<?php 
                    elseif(get_row_layout()=="accordion"):
?>

<?php 
                      if(have_rows("service_accordion")):
?>
                      <div class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<?php
                        while(have_rows("service_accordion")): the_row();
?>
                        <div class="accordion-item" data-accordion-item>
                          <a href="#" class="accordion-title"><?php the_sub_field("accordion_title"); ?></a>
                          <div class="accordion-content" data-tab-content>
                          <?php the_sub_field("accordion_content"); ?>
                          </div> <!-- .accordion-content -->
                        </div> <!-- .accordion-item -->
<?php
                        endwhile; // have_rows('service_accordion')
?>
                      </div> <!-- .overview-accordion -->
<?php 
                      endif; // have_rows('service_accordion')
?>
<?php
                    endif; // get_row_layout
?>                                          
<?php
                    endwhile; // have_rows
                  endif; // have_rows
?>
                                                                                                                                                    
                </div> <!-- .detail-main -->
              </div> <!-- columns -->
              <div class="small-12 large-4 columns">
                <div class="overview-sidebar">
                <!-- Begin Flexible Content -->

<?php
                    $image = get_field("tax_guide_image");
                    $image_url = $image['url'];
                    $image_alt = $image['alt'];
?>
                  <figure class="tax-guide-image">
                    <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                  </figure>

                  <div class="sidebar-visual-editor tax-guide-description">
                    <?php the_field("tax_guide_description"); ?>
                  </div> <!-- .sidebar-visual-editor -->


                  <div class="quantity-price">
                    <div class="quantity-price-label">                        
                        <?php the_field("quantity_price_label"); ?>
                    </div> <!-- .quantity-label -->    
<?php                    

                        if(have_rows("quantity_pricing")):
?>
                    <div class="quantity-price-list">
<?php                                              
                          while(have_rows("quantity_pricing")): the_row();
?>
                      <span class="quantity-price">
                        <?php the_sub_field("quantity_price"); ?>
                      </span> <!-- .quantity-price --> 
                      <span class="additional-information">
                        <?php the_sub_field("additional_information"); ?>
                      </span> <!-- .additional-information -->                            
<?php
                          endwhile;
?>
                    </div> <!-- quantity-price-list -->
<?php                                              
                        endif;
?>
                  </div> <!-- .quantity-price -->

                  <div class="paypal">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="J7Q4KH23E7AGA">
                    <table>
                    <tr><td><input type="hidden" name="on0" value="RTA"><h3>Relocation Tax Advisor</h3></td></tr><tr><td><select name="os0">
                      <option value="1-5 each">1-5 each $24.95 USD</option>
                      <option value="6-199 each">6-199 each $7.25 USD</option>
                      <option value="200+ each">200+ each $6.25 USD</option>
                      <option value="PDF Books (100 minimum order) each">PDF Books (100 minimum order) each $6.00 USD</option>
                    </select> </td></tr>
                    </table>
                    <input type="hidden" name="currency_code" value="USD">
                    <input type="submit" value="Order Online Now">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>

                  </div> <!-- .paypal -->                  

<?php  
                      if(have_rows("buttons")):

                        while(have_rows("buttons")): the_row();
?>

<?php           
                          if( get_sub_field('button_type') == 'External Link' ):
?>
                  <a class="overview-form-button sidebar-button" href="<?php the_sub_field('external_link'); ?>" target="_blank"><?php the_sub_field("button_text"); ?></a>
<?php 
                          elseif(get_sub_field('sidebar_button_type') == 'Internal Link' ):
?>
                  <a class="overview-form-button sidebar-button" href="<?php the_sub_field('sidebar_button_link'); ?>"><?php the_sub_field("button_text"); ?></a>
<?php 
                          endif; // get_sub_field('intro_button_type')
                        endwhile; // while(have_rows)
                      endif; // if(have_rows)
?>
                  <div class="sidebar-visual-editor">
                    <?php the_field("purchase_alternative"); ?>
                  </div> <!-- .sidebar-visual-editor -->

                </div> <!-- .overview-sidebar -->              
              </div> <!-- .columns -->
        </div> <!-- .columns -->
      </div> <!-- .row -->
    </div> <!-- .mobility-software-main --> 


<!-- Sidebar Forms -->
<?php
            if(have_rows("sidebar_content")):
              while(have_rows("sidebar_content")): the_row();

                if(get_row_layout() == "button"):
                  if(have_rows("sidebar_buttons")):

                    while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                      if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
    <div class="reveal full" id="<?php the_sub_field('sidebar_form_name'); ?>" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_sub_field('sidebar_form_title'); ?></h3>            
            <div class="row">
              <?php the_sub_field('sidebar_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div>  

<?php 
                      endif; // get_sub_field('intro_button_type')
                    endwhile;
                  endif;
                endif; // get_row_layout()
              endwhile; // while(have_rows("sidebar_content")
            endif; // if(have_rows("sidebar_content"))
?>      
          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
