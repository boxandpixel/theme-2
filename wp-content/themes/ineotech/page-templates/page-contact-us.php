<?php
/**
 * Template Name: Contact Us
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior contact-us">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    
      <div class="contact-overview">

        <div class="layer-up">  
          <div class="row">
            <div class="small-10 small-centered medium-8 medium-centered columns">
              <div class="interior-intro">
                
                <h2><?php the_field('page_title') ?></h2>

                <div class="row">
                  <div class="small-12 columns form-overview">  
                  <?php the_field("contact_us_form") ?>
                  </div> <!-- .columns -->
                </div> <!-- .row-->     

                <div class="row">
                  <div class="small-12 columns"> 
                  <div class="visual-editor contact-content"> 
                    <?php the_field("contact_us_content") ?>
                  </div> <!-- .visual-editor -->
                  </div> <!-- .columns -->
                </div> <!-- .row-->                                  
                                            
              </div> <!-- .about-intro -->
            </div> <!-- .columns -->
          </div> <!-- .row -->
        </div> <!-- .layer-up -->
               
      </div> <!-- .contact-overview -->
    

    

          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
