<?php

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-detail mobility-solutions test">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'knowledge-resources-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div id="main">


      <div class="row">
        <div class="small-12 columns">

          <div class="row">
              <div class="small-12 large-8 columns">
                <div class="detail-main">
                  <h1><?php the_title(); ?></h1>

<?php
                  if(have_rows("mobility_solutions_content")):
                    while(have_rows("mobility_solutions_content")): the_row();
?>

<?php
                    if(get_row_layout()=="visual_editor"):
?>
                      <?php the_sub_field("mobility_visual_editor"); ?>
<?php
                    elseif(get_row_layout()=="image"):

                      $image = get_sub_field("mobility_image");
                      $image_url = $image['url'];
                      $image_alt = $image['alt'];
?>
                      <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="mobility-image">
<?php 
                    elseif(get_row_layout()=="accordion"):
?>

<?php 
                      if(have_rows("mobility_accordion")):
?>
                      <div class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<?php
                        while(have_rows("mobility_accordion")): the_row();
?>
                        <div class="accordion-item" data-accordion-item>
                          <a href="#" class="accordion-title"><?php the_sub_field("accordion_title"); ?></a>
                          <div class="accordion-content" data-tab-content>
                          <?php the_sub_field("accordion_content"); ?>
                          </div> <!-- .accordion-content -->
                        </div> <!-- .accordion-item -->
<?php
                        endwhile; // have_rows('mobility_accordion')
?>
                      </div> <!-- .overview-accordion -->
<?php 
                      endif; // have_rows('mobility_accordion')
?>
<?php
                    endif; // get_row_layout
?>                                          
<?php
                    endwhile; // have_rows
                  endif; // have_rows
?>

<?php
            $mobility_pdf = get_field("mobility_solutions_pdf");
            $mobility_pdf_url = $mobility_pdf['url'];   

                  if($mobility_pdf):
?>
                  <a class="button-white" href="<?php echo $mobility_pdf_url; ?>">Download PDF</a>
<?php 
                  endif;
?>
                                                                                                                                                   
                </div> <!-- .detail-main -->
              </div> <!-- columns -->
              <div class="small-12 large-4 columns">
                <div class="overview-sidebar">

                  <h2>View or download our other articles:</h2> 
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 5,
        //'offset' => 1,
        'post_type' => 'mobility-solutions', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                  <div class="mobility-sidebar-article">
<?php
            $mobility_pdf_image = get_field("mobility_solutions_pdf_image");
            $mobility_pdf_image_url = $mobility_pdf_image['url'];
            $mobility_pdf_image_alt = $mobility_pdf_image['alt'];    
?>

<?php
            $mobility_pdf = get_field("mobility_solutions_pdf");
            $mobility_pdf_url = $mobility_pdf['url'];   
?>
                    <img src="<?php echo $mobility_pdf_image_url; ?>" alt="<?php echo $mobility_pdf_image_alt; ?>">                    
                    <h1><?php the_title(); ?></h1>
                    <div class="mobility-meta">
                      <a href="<?php the_permalink(); ?>">Read the article</a> | <a href="<?php echo $mobility_pdf_url; ?>">Download PDF</a>
                    </div> <!-- .mobility-meta -->
                  </div> <!-- .mobility-sidebar-article -->
<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>                
                </div> <!-- .overview-sidebar -->              
              </div> <!-- .columns -->
        </div> <!-- .columns -->
      </div> <!-- .row -->
     
    </div> <!-- #main --> 


<!-- Sidebar Forms -->
<?php
            if(have_rows("sidebar_content")):
              while(have_rows("sidebar_content")): the_row();

                if(get_row_layout() == "button"):
                  if(have_rows("sidebar_buttons")):

                    while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                      if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
    <div class="reveal full" id="<?php the_sub_field('sidebar_form_name'); ?>" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_sub_field('sidebar_form_title'); ?></h3>            
            <div class="row">
              <?php the_sub_field('sidebar_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div>  

<?php 
                      endif; // get_sub_field('intro_button_type')
                    endwhile;
                  endif;
                endif; // get_row_layout()
              endwhile; // while(have_rows("sidebar_content")
            endif; // if(have_rows("sidebar_content"))
?>      
          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
