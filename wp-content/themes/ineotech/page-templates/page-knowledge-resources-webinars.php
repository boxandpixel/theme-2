<?php
/**
 * Template Name: Webinars
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-detail webinars">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'knowledge-resources-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div id="main">

 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        //'offset' => 0,
        'post_type' => 'webinars', 
        'orderby' => 'date',
        'order' => 'asc'
      )); 
      

      if( $posts ):
?>

      <div class="row">
        <div class="small-12 columns">
          <h1><?php the_field("page_title"); ?></h1>
        </div> <!-- .columns -->
      </div> <!-- .row -->
<?php
        foreach( $posts as $post ):     
          setup_postdata($post);
?>          
      <div class="webinar">
        <div class="row">
          <div class="small-12 large-8 columns">
            <span class="webinar-month"><?php the_field("webinar_month"); ?></span>

            <div class="webinar-content">
<?php
          if(have_rows("webinar_content")):
            while(have_rows("webinar_content")): the_row();

              if(get_row_layout() == "visual_editor"):

                the_sub_field("webinar_visual_editor");

              elseif(get_row_layout() == "button"):

                if(have_rows("registration_button")):
?>
            <div class="webinar-registration-buttons">
<?php                                 
                  while(have_rows("registration_button")): the_row();
?>
            <a href="<?php the_sub_field("button_link"); ?>" class="button-white" target="_blank"><?php the_sub_field("button_text"); ?></a>
<?php                  
                  endwhile;
?>
            </div> <!-- .webinar-registration-buttons -->
<?php                              
                endif;

              endif;

            endwhile;
          endif;
?>
            </div> <!-- .webinar-content -->
<!-- Begin Read More -->
<?php                      
          $read_more = get_field("webinar_read_more");
          if($read_more != ""):
?>
            <div class="webinar-read-more">
              <a href="#" class="see-more">See More</a>
              <div class="webinar-more">
                <?php the_field("webinar_read_more"); ?>
              </div> <!-- .webinar-more -->
              <a href="#" class="see-less">See Less</a>
            </div> <!-- .webinar-read-more -->
<?php
          endif;
?>
<!-- Being Sponsors -->
<?php
          if(have_rows("webinar_sponsors")):
?>
            <div class="webinar-sponsors">
<?php                        
            while(have_rows("webinar_sponsors")): the_row();
?>
              <h2><?php the_sub_field("sponsor_type"); ?></h2>
<?php
              if(have_rows("sponsor")):
?>
              <div class="sponsors">
<?php                
                while(have_rows("sponsor")): the_row();
                  $sponsor_image = get_sub_field("sponsor_image");
                  $sponsor_image_url = $sponsor_image['url'];
                  $sponsor_image_alt = $sponsor_image['alt'];                
?>
                <div class="sponsor">                          
                  <a href="<?php the_sub_field("sponsor_link"); ?>" target="_blank">     
                    <img src="<?php echo $sponsor_image_url; ?>" alt="<?php echo $sponsor_image_url; ?>">
                  </a>
                </div> <!-- .sponsor -->                  
<?php
                endwhile;
?>
              </div> <!-- .sponsors-->
<?php                                
              endif;
            endwhile;
?>
            </div> <!-- .webinar-sponsors -->
<?php                        
          endif;
?>          

          </div> <!-- .columns -->
          <div class="small-12 large-4 columns">
<?php
          if(have_rows("webinar_images")):
            while(have_rows("webinar_images")): the_row();

              $webinar_image = get_sub_field("webinar_image");
              $webinar_image_url = $webinar_image['url'];
              $webinar_image_alt = $webinar_image['alt'];
?>
            <figure class="webinar-image">
              <img src="<?php echo $webinar_image_url; ?>" alt="<?php echo $webinar_image_alt; ?>">
              <figcaption><?php the_sub_field("webinar_name"); ?></figcaption>
            </figure>
<?php                        

            endwhile;
          endif;
?>
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .webinar -->
<?php
          wp_reset_postdata();
        endforeach;
?>      
<?php
      endif;       
?>

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
