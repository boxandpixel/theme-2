<?php
/**
 * Template Name: Financial Solutions Detail
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior service-detail detail-financial">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'financial-solutions-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div class="mobility-software-main" id="main">


      <div class="row">
        <div class="small-11 small-centered columns">

          <div class="row">
              <div class="small-12 large-8 columns">
                <div class="detail-main">
                  <h1><?php the_field("service_page_title"); ?></h1>

<?php
                  if(have_rows("service_page_content")):
                    while(have_rows("service_page_content")): the_row();
?>

<?php
                    if(get_row_layout()=="visual_editor"):
?>
                      <?php the_sub_field("service_visual_editor"); ?>
<?php
                    elseif(get_row_layout()=="image"):

                      $image = get_sub_field("service_image");
                      $image_url = $image['url'];
                      $image_alt = $image['alt'];
?>
                      <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php
                    elseif(get_row_layout()=="text"):
?>
                      <?php the_sub_field("service_text"); ?>

<?php 
                    elseif(get_row_layout()=="accordion"):
?>

<?php 
                      if(have_rows("service_accordion")):
?>
                      <div class="accordion" data-accordion  data-multi-expand="true" data-allow-all-closed="true">
<?php
                        while(have_rows("service_accordion")): the_row();
?>
                        <div class="accordion-item" data-accordion-item>
                          <a href="#" class="accordion-title"><?php the_sub_field("accordion_title"); ?></a>
                          <div class="accordion-content" data-tab-content>
                          <?php the_sub_field("accordion_content"); ?>
                          </div> <!-- .accordion-content -->
                        </div> <!-- .accordion-item -->
<?php
                        endwhile; // have_rows('service_accordion')
?>
                      </div> <!-- .overview-accordion -->
<?php 
                      endif; // have_rows('service_accordion')
?>
<?php
                    endif; // get_row_layout
?>                                          
<?php
                    endwhile; // have_rows
                  endif; // have_rows
?>
                                                                                                                                                    
                </div> <!-- .detail-main -->
              </div> <!-- columns -->
              <div class="small-12 large-4 columns">

                <div class="overview-sidebar">
                <!-- Begin Flexible Content -->
<?php
                if(have_rows("sidebar_content")):
                  while(have_rows("sidebar_content")): the_row();

                    if(get_row_layout() == "image"):
                    $sidebar_image = get_sub_field("sidebar_image");
                    $sidebar_image_url = $sidebar_image['url'];
                    $sidebar_image_alt = $sidebar_image['alt'];
?>
                  <img src="<?php echo $sidebar_image_url; ?>" alt="echo $sidebar_image_alt;">

<?php 
                    elseif(get_row_layout() == "visual_editor"):
?>
                  <div class="sidebar-visual-editor">
                    <?php the_sub_field("sidebar_visual_editor"); ?>
                  </div> <!-- .sidebar-visual-editor -->
<?php
                    elseif(get_row_layout() == "button"):
  
                      if(have_rows("sidebar_buttons")):

                        while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                          if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
                  <a data-open="<?php the_sub_field('sidebar_form_name'); ?>" class="overview-form-button sidebar-button"><?php the_sub_field("sidebar_button_text"); ?></a>
<?php 
                          elseif(get_sub_field('sidebar_button_type') == 'Page Link' ):
?>
                  <a class="overview-form-button sidebar-button" href="<?php the_sub_field('sidebar_button_link'); ?>"><?php the_sub_field("sidebar_button_text"); ?></a>

<?php
                    endif; // get_sub_field('intro_button_type')
                  endwhile; // while(have_rows)
                endif; // if(have_rows)


                    endif; // get_row_layout
                  endwhile; // while(have_rows)
                endif; // if(have_rows)
?>                 
                </div> <!-- .overview-sidebar -->               
              </div> <!-- .columns -->
        </div> <!-- .columns -->
      </div> <!-- .row -->
    </div> <!-- .mobility-software-main --> 


<!-- Sidebar Forms -->
<?php
            if(have_rows("sidebar_content")):
              while(have_rows("sidebar_content")): the_row();

                if(get_row_layout() == "button"):
                  if(have_rows("sidebar_buttons")):

                    while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                      if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
    <div class="reveal full" id="<?php the_sub_field('sidebar_form_name'); ?>" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_sub_field('sidebar_form_title'); ?></h3>            
            <div class="row">
              <?php the_sub_field('sidebar_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div>  

<?php 
                      endif; // get_sub_field('intro_button_type')
                    endwhile;
                  endif;
                endif; // get_row_layout()
              endwhile; // while(have_rows("sidebar_content")
            endif; // if(have_rows("sidebar_content"))
?>      
          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
