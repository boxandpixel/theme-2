<?php
/**
 * Template Name: About
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior about">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

      <div class="about-overview">
        <div class="lines"></div>

        <div class="layer-up">
          
          <div class="secondary-nav">

            <div id="about-nav-title">
              <span class="overview-page"><?php the_title(); ?></span>
              <span class="overview-title"><?php the_field('about_navigation_title'); ?></span>
            </div> <!-- #service-nav-title -->

          </div> <!-- .about-nav -->
          <div class="row">
            <div class="small-10 small-centered medium-8 medium-centered columns">
              <div class="interior-intro">
                <span class="site-title"><?php the_field('about_intro_ineo'); ?></span>
                <h1><?php the_field('about_intro_title') ?></h1>
                <?php the_field('about_intro'); ?>              
              </div> <!-- .about-intro -->
            </div> <!-- .columns -->
          </div> <!-- .row -->
        </div> <!-- .layer-up -->
        <div class="js-scroll-wrap down-arrow" data-active="on">
          <span class="arrow-btn arrow-btn-down js-scroll-down" data-scroll-to-element="service-overview-main" data-scroll-speed="1750">
            <img src="/wp-content/themes/ineotech/assets/images/arrow-mobile.svg" alt="" width="30" height="30">
          </span>
        </div> <!-- .intro-arrow -->        
      </div> <!-- .service-overview -->

    <div class="about-main" id="main">
      <div class="row">
        <div class="small-10 small-centered columns">
<?php
        if(have_rows('about_sections')):
          while(have_rows('about_sections')): the_row();

            if(get_row_layout() == "about_text_area"):
?>
          <div class="about-section">          
            <h2><?php the_sub_field("about_title"); ?></h2>
            <?php the_sub_field("about_text"); ?>
          </div> <!-- .about-section -->

<?php
            elseif(get_row_layout() == 'about_list_items'):  
?>
          <div class="about-section">          
            <h2><?php the_sub_field("about_title"); ?></h2>
<?php
              if(have_rows('about_list')):
?>
            <ul class="about-list">
<?php                            
                while(have_rows('about_list')): the_row();
?>
              <li>
                <h3><?php the_sub_field('about_list_title'); ?></h3>
                <?php the_sub_field("about_list_text"); ?>
              </li>                
<?php
                endwhile; // about_list
?>
            </ul>
<?php                            
              endif; // about_list
           
?>                                          
          </div> <!-- .about-section -->                    
<?php
            endif; // get_row_layout             
          endwhile; // about_sections
        endif; // about_sections
?>
        </div> <!-- .columns -->
      </div> <!-- .row -->

      <div class="row">
        <div class="small-12 columns">

 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        'offset' => 0,
        'post_type' => 'employee', 
        'orderby' => 'title',
        'order' => 'asc'
      )); 
      

      if( $posts ):

?>
 
      <div class="about-employees">

        <ul class="employees">
          
    
<?php
      foreach( $posts as $post ):     
        setup_postdata($post);

        $employee_image = get_field("employee_image");
        $employee_image_alt = $employee_image['alt'];
        $employee_image_url = $employee_image['url']; 

        
        $terms = wp_get_post_terms( $post->ID, 'alpha'); 
?>

        <li data-flip-category="<?php foreach($terms as $term){echo $term->name;} ?>">
          <div class="row">
            <div class="small-10 small-centered columns">
              <img src="<?php echo $employee_image_url; ?>" alt="">
              <h3><?php the_field('employee_name'); ?></h3>
              <span class="emp-title"><?php the_field('employee_title'); ?></span>
              <?php the_field('employee_description'); ?>
            </div> 
          </div> 
        </li>

<?php
        wp_reset_postdata();
      endforeach;
?>
        </ul>
      </div> <!-- .about-employees -->
      

<?php endif; ?>
               
        </div> <!-- .columns -->
      </div> <!-- .row -->

      <div class="about-affiliations">
        <div class="row">
          <div class="small-10 small-centered columns">
            <h3><?php the_field("affiliations_title"); ?></h3>
            <div class="row">
<?php 
            if(have_rows("affiliations_list")):
              while(have_rows("affiliations_list")): the_row();
?>                            
              <div class="small-12 large-3 columns affiliation-each">
<?php                 
                $aff_image = get_sub_field("affiliation");
                $aff_image_url = $aff_image['url'];
                $aff_image_alt = $aff_image['alt'];
?>
                <img src="<?php echo $aff_image_url; ?>" alt="<?php echo $aff_image_alt; ?>">  


              </div> <!-- .columns -->
<?php 
              endwhile;
            endif;
?>              
            </div> <!-- .row -->
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .about-affiliations -->
    </div> <!-- .about-main --> 

          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
