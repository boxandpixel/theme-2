<?php
/**
 * Template Name: Service Overview
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior service-overview">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

<?php 
    $overview_image = get_field('overview_intro_image');
    $overvie_image_alt = $overview_image['alt'];
    $overview_image_url = $overview_image['url'];
?>
      <div class="interior-overview" style="background: url('<?php echo $overview_image_url; ?>') center bottom no-repeat; background-size: cover;">
        <div class="lines"></div>

        <div class="layer-up">
          <div class="secondary-nav">

            <div id="service-nav-title">
              <span class="overview-page"><?php the_title(); ?></span>
              <span class="overview-title"><?php the_field('overview_navigation_title'); ?></span>
            </div> <!-- #service-nav-title -->
<?php 
            if(is_page('Financial Solutions')){
              wp_nav_menu( array( 'theme_location' => 'financial-solutions-menu' ) );

            } elseif(is_page('Mobility Software')) {
              wp_nav_menu( array( 'theme_location' => 'mobility-software-menu' ) );
            
            } elseif(is_page('Tax Services')) {
              wp_nav_menu( array( 'theme_location' => 'tax-services-menu' ) );
            }
?>
          </div> <!-- .service-nav -->
          <div class="row">
            <div class="small-10 small-centered medium-8 medium-centered columns">
              <div class="interior-intro">
                <span class="site-title"><?php the_field('overview_intro_ineo'); ?></span>
                <h1><?php the_title(); ?></h1>
                <?php the_field('overview_intro'); ?>
                <!-- test new buttons -->
<?php
                if(have_rows("overview_intro_buttons")):

                  while(have_rows("overview_intro_buttons")): the_row();
?>

<?php           
                    if( get_sub_field('intro_button_type') == 'Lightbox Link' ):
?>
                <a data-open="<?php the_sub_field('intro_form_name'); ?>" class="overview-form-button"><?php the_sub_field("intro_button_text"); ?></a>
<?php 
                    elseif(get_sub_field('intro_button_type') == 'Page Link' ):
?>
                <a class="overview-form-button" href="<?php the_sub_field('intro_button_link'); ?>"><?php the_sub_field("intro_button_text"); ?></a>





<?php
                    endif; // get_sub_field('intro_button_type')
                  endwhile; // while(have_rows)
                endif; // if(have_rows)
?>

                <!-- end test new buttons -->

  <?php
                $overview_form_button = get_field('overview_form_button');
                if($overview_form_button):
  ?>    
                <a data-open="intro-financial-form" class="overview-form-button"><?php echo $overview_form_button; ?></a>
  <?php 
                endif;
  ?>              
              </div> <!-- .overview-intro -->
            </div> <!-- .columns -->
          </div> <!-- .row -->
        </div> <!-- .layer-up -->
        <div class="js-scroll-wrap down-arrow" data-active="on">
          <span class="arrow-btn arrow-btn-down js-scroll-down" data-scroll-to-element="service-overview-main" data-scroll-speed="1750">
            <img src="/wp-content/themes/ineotech/assets/images/arrow-mobile.svg" alt="" width="30" height="30">
          </span>
        </div> <!-- .intro-arrow -->        
      </div> <!-- .service-overview -->

    <div class="service-overview-main" id="main">
      <div class="row">
        <div class="small-11 small-centered medium-12 columns">

          <div class="row">
              <div class="small-12 large-8 columns">
                <div class="overview-main">
                  <?php the_field('overview_main_content'); ?>
                </div> <!-- .overview-main -->
              </div> <!-- columns -->
              <div class="small-12 large-4 columns">
<?php
              $sidebar_image = get_field("sidebar_image");
              $sidebar_image_url = $sidebar_image['url'];
              $sidebar_image_alt = $sidebar_image['alt'];
?> 
                <div class="overview-sidebar">
                  <img src="<?php echo $sidebar_image_url; ?>" alt="<?php echo $sidebar_image_alt; ?>">
                  <div class="sidebar-visual-editor">
                    <?php the_field('sidebar_wysiwyg'); ?>
                  </div> <!-- sidebar-wysiwyg -->

<?php
                if(have_rows("sidebar_buttons")):

                  while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                    if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
                <a data-open="<?php the_sub_field('sidebar_form_name'); ?>" class="overview-form-button sidebar-button"><?php the_sub_field("sidebar_button_text"); ?></a>
<?php 
                    elseif(get_sub_field('sidebar_button_type') == 'Page Link' ):
?>
                <a class="overview-form-button sidebar-button" href="<?php the_sub_field('sidebar_button_link'); ?>"><?php the_sub_field("sidebar_button_text"); ?></a>





<?php
                    endif; // get_sub_field('intro_button_type')
                  endwhile; // while(have_rows)
                endif; // if(have_rows)
?>
<?php if(is_page("tax-services")):
?>
                  <div class="custom-payment">
                  <div class="paypal">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
                      <h3>Payment Description</h3>
                    <input type="hidden" name="cmd" value="_s-xclick">
                    <input type="hidden" name="hosted_button_id" value="FK32F5NDVYU2A">
                    <table>
                    <tr><td>What are you paying for?</td></tr><tr><td><input type="text" name="item_name" maxlength="200"></td></tr>
                    </table>
                    <input type="submit" value="Make A Payment">
                    <img alt="" border="0" src="https://www.paypalobjects.com/en_US/i/scr/pixel.gif" width="1" height="1">
                    </form>

                  </div> <!-- .paypal -->
                </div> <!-- .custom-payment -->
<?php endif; ?>                                   
                </div> <!-- .overview-sidebar -->               
              </div> <!-- .columns -->
        </div> <!-- .columns -->
      </div> <!-- .row -->
    </div> <!-- .service-overview-main --> 

    <div class="reveal full" id="intro-financial-form" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_field('overview_form_title'); ?></h3>
            <div class="row">
              <?php the_field('overview_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->            
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div> 


<!-- Intro Forms -->
<?php
                if(have_rows("overview_intro_buttons")):

                  while(have_rows("overview_intro_buttons")): the_row();
?>

<?php           
                    if( get_sub_field('intro_button_type') == 'Lightbox Link' ):
?>
    <div class="reveal full" id="<?php the_sub_field('intro_form_name'); ?>" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_sub_field('intro_form_title'); ?></h3>            
            <div class="row">
              <?php the_sub_field('intro_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div>  

<?php 
                    endif; // get_sub_field('intro_button_type')
                  endwhile;
                endif;
?> 

<!-- Sidebar Forms -->
<?php
                if(have_rows("sidebar_buttons")):

                  while(have_rows("sidebar_buttons")): the_row();
?>

<?php           
                    if( get_sub_field('sidebar_button_type') == 'Lightbox Link' ):
?>
    <div class="reveal full" id="<?php the_sub_field('sidebar_form_name'); ?>" data-reveal>
      <div class="row form-overlay">
          <div class="small-12 medium-10 medium-centered columns">
            <h3><?php the_sub_field('sidebar_form_title'); ?></h3>            
            <div class="row">
              <?php the_sub_field('sidebar_form_content'); ?>
            </div> <!-- .row -->
            <div class="row">
              <div class="small-12 columns text-center">
                <button class="form-close" data-close type="button">Close</button>
              </div> <!-- .columns -->
            </div> <!-- .row -->
          </div> <!-- .columns -->
      </div> <!-- .row -->
      <button class="close-button" data-close aria-label="Close modal" type="button">
        <span aria-hidden="true">&times;</span>
      </button>      
    </div>  

<?php 
                    endif; // get_sub_field('intro_button_type')
                  endwhile;
                endif;
?>             

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
