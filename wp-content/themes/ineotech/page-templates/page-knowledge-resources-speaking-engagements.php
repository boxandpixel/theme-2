<?php
/**
 * Template Name: Speaking Engagements
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-detail speaking-engagements">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'knowledge-resources-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div id="main">

 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        //'offset' => 0,
        'post_type' => 'speaking-engagements', 
        'orderby' => 'date',
        'order' => 'asc'
      )); 
      

      if( $posts ):
?>

      <div class="row">
        <div class="small-12 columns">
          <h1><?php the_field("page_title"); ?></h1>
        </div> <!-- .columns -->
      </div> <!-- .row -->
<?php
        foreach( $posts as $post ):     
          setup_postdata($post);
?>          
      <div class="speaking-engagement">
        <div class="row">
          <div class="small-12 large-8 columns">
            <div class="speaking-engagement-location">
              <span class="speaking-location"><?php the_field("speaking_engagement_location"); ?></span> | <span class="speaking-date"><?php the_field("speaking_engagement_date"); ?></span> | <span class="speaking-date"><?php the_field("speaking_engagement_time"); ?></span>
            </div> <!-- .speaking-engagement-location -->

            <div class="speaking-engagement-content">
              <h2><?php the_title(); ?></h2>

<?php 
              if(have_rows("speaking_engagement_information")):
                while(have_rows("speaking_engagement_information")): the_row();
?>              
<?php
                  if(get_row_layout() == "featured_speakers"):
                    if(have_rows("featured_speakers")):
?>
              <div class="featured-speakers"> 
                <h3>Featured Speakers</h3>
<?php                                   
                      while(have_rows("featured_speakers")): the_row();
?>
                <div class="speakers">
                  <span class="speaker-name"><?php the_sub_field("speaker_name"); ?></span>
                  <span><?php the_sub_field("speaker_title"); ?></span>
                  <span><?php the_sub_field("speaker_email"); ?></span>
                </div> <!-- .speakers -->
             
<?php                        

                      endwhile;
?>
              </div> <!-- .featured-speakers --> 
<?php                       
                    endif;
?>              
<?php
                  elseif(get_row_layout() == "visual_editor"):
?>
              <div class="visual-editor">
                  <?php the_sub_field("visual_editor"); ?>
              </div> <!-- .visual-editor -->                    

<?php
                  endif;
                endwhile;
              endif;
?>              
            </div> <!-- .speaking-engagement-content -->

<!-- Being Sponsors -->        

          </div> <!-- .columns -->
          <div class="small-12 large-4 columns">
<?php 
              if(have_rows("speaking_engagement_information")):
                while(have_rows("speaking_engagement_information")): the_row();

                  if(get_row_layout() == "featured_speakers"):
                    if(have_rows("featured_speakers")):
                      while(have_rows("featured_speakers")): the_row();

              $speaker_image = get_sub_field("speaker_image");
              $speaker_image_url = $speaker_image['url'];
              $speaker_image_alt = $speaker_image['alt'];
?>
            <figure class="speaker-image">
              <img src="<?php echo $speaker_image_url; ?>" alt="<?php echo $speaker_image_alt; ?>">
              <figcaption><?php the_sub_field("speaker_name"); ?></figcaption>
            </figure>
<?php                        
                  
                      endwhile;
                    endif;
                  endif;
                endwhile;
              endif;

?>
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .webinar -->
<?php
          wp_reset_postdata();
        endforeach;
?>      
<?php
      endif;       
?>

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
