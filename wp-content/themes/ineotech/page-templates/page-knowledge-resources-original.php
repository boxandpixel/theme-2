<?php
/**
 * Template Name: Knowledge Resources Original
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-resources">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>
       
    <div id="main">

      <div class="knowledge-intro">
        <div class="row">
          <div class="small-12 columns">
            <?php the_field("intro_ineo"); ?>
            <h1><?php the_title(); ?></h1>
            <h2><?php the_field("intro_text"); ?></h2>
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .knowledge-intro -->

      <div class="knowledge-grid">

        <div class="row">

          <div class="small-12 columns">
            <div class="row">
              <div class="small-12 medium-10 medium-centered large-12 columns">
                <div class="row">
                  <div class="large-8 large-push-4 small-12 columns cols-2px">
                    <a class="panel mobility" href="/knowledge-resources/mobility-solutions">Mobility Solutions</a>
                  </div> <!-- cols -->

                  <div class="large-4 large-pull-8 small-12 columns cols-2px">
                    <a class="panel webinars" href="/knowledge-resources/webinars">Webinars</a>
                  </div> <!-- cols -->
                </div> <!-- .row -->
              </div> <!-- .cols -->
            </div> <!-- .row -->
          </div> <!-- .cols -->                             
        </div> <!-- .row -->

        <div class="row">

          <div class="small-12 medium-10 medium-centered large-12 columns">

            <div class="row">

              <div class="large-4 medium-12 columns">
                <div class="row">
                  <div class="small-12 columns cols-2px">
                    <a class="panel speaking" href="/knowledge-resources/speaking-engagements">Speaking Engagements</a>
                  </div> <!-- .small-12 -->
                  <div class="small-12 columns cols-2px">
                    <p class="panel tax-guide">Tax Guide</p>
                  </div> <!-- .small-12 -->              
                </div> <!-- .row -->
              </div> <!-- cols -->

              <div class="large-8 medium-12 columns">
                <div class="row">
                  <div class="small-12 columns cols-2px">
                    <p class="panel email">Email</p>
                  </div> <!-- .cols -->
                  <div class="small-12 columns">
                    <div class="row cols-2px">
                      <div class="small-12 medium-6 columns cols-2px">
                        <a class="panel tax-updates" href="/knowledge-resources/tax-updates">Tax Updates</a>
                      </div> <!-- cols -->
                      <div class="small-12 medium-6 columns cols-2px">
                        <a class="panel subject-matters" href="/knowledge-resources/subject-matters">Subject Matters</a>
                      </div> <!-- cols -->                  
                    </div> <!-- .row -->
                  </div> <!-- .cols -->
                </div> <!-- .row -->
              </div> <!-- .cols --> 
            </div> <!-- .row -->
          </div> <!-- .columns -->
        </div> <!-- .row -->

      </div> <!-- .knowledge-grid -->    


    </div> <!-- #main --> 


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
