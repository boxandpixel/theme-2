<?php
/**
 * Template Name: Privacy Policy
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior privacy-policy">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    
      <div id="main">


          <div class="row">
            <div class="small-10 small-centered medium-8 medium-centered columns">
              <h1 class="page-title"><?php the_field('privacy_policy_title'); ?></h1>
              <?php the_field("privacy_policy_content"); ?>
            </div> <!-- .columns -->
          </div> <!-- .row -->

               
      </div> <!-- #main -->
    

    

          

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
