<?php
/**
 * Template Name: Tax Updates
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-detail tax-updates">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

     <?php wp_nav_menu( array( 'theme_location' => 'knowledge-resources-menu' ) ); ?>

    </div> <!-- .service-nav -->    
    
    <div id="main">

 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        //'offset' => 0,
        'post_type' => 'tax-updates', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
?>
      
      <div class="tax-updates-title">
        <div class="row">
          <div class="small-12 columns">
            <h1><?php the_field("page_title"); ?></h1>
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .tax-updates-title -->
<?php
        foreach( $posts as $post ):     
          setup_postdata($post);

            $post_id = get_the_ID();
?>          
      <div class="tax-update" id="<?php echo $post_id; ?>">
        <div class="row">
          <div class="small-12 large-8 columns last">
            <h2><?php the_title(); ?></h2>

            <div class="visual-editor">
<?php
            if(have_rows("knowledge_resources_content")):
              while(have_rows("knowledge_resources_content")): the_row();

                if(get_row_layout() == "visual_editor"):

                  the_sub_field("visual_editor");

                elseif(get_row_layout() == "image"):

                  the_sub_field("image");

                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];
?>
              <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
<?php 
                elseif(get_row_layout() == "accordion"):

                  if(have_rows("accordion")):
                    while(have_rows("accordion")): the_row();
?>
                      <div class="accordion" data-accordion data-multi-expand="true" data-allow-all-closed="true">
<?php
                        while(have_rows("accordion")): the_row();
?>
                        <div class="accordion-item" data-accordion-item>
                          <a href="#" class="accordion-title"><?php the_sub_field("accordion_title"); ?></a>
                          <div class="accordion-content" data-tab-content>
                          <?php the_sub_field("accordion_content"); ?>
                          </div> <!-- .accordion-content -->
                        </div> <!-- .accordion-item -->
<?php
                        endwhile; // have_rows('service_accordion')
?>
                      </div> <!-- .accordion -->
<?php                      
                    endwhile;
                  endif;

                elseif(get_row_layout() == "pdf"):
                  $pdf = get_sub_field("pdf");
                  $pdf_url = $pdf['url'];
?>
              <a href="<?php echo $pdf_url; ?>" class="button-white">Download PDF</a>                  

<?php
                endif; // get_row_layout
              endwhile;
            endif;

?>                
            </div> <!-- .visual-editor -->

          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .tax-update -->
      
<?php
          wp_reset_postdata();
        endforeach;
?>      
<?php
      endif;       
?>

 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
