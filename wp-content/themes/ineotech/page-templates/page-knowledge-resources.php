  <?php
/**
 * Template Name: Knowledge Resources
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior knowledge-resources">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>
       
    <div id="main">

      <div class="knowledge-intro">
        <div class="row">
          <div class="small-12 columns">
            <span class="site-title"><?php the_field("intro_ineo"); ?></span>
            <h1><?php the_title(); ?></h1>
            <h2><?php the_field("intro_text"); ?></h2>
          </div> <!-- .columns -->
        </div> <!-- .row -->
      </div> <!-- .knowledge-intro -->

      <div class="knowledge-grid">

        <div class="row">

          <div class="small-11 small-centered medium-12 columns">
            <div class="row mobility-webinars">
              <div class="small-12 medium-10 medium-centered large-12 columns">
                <div class="row">
                  <div class="large-8 large-push-4 small-12 columns cols-2px">
                    <div class="knowledge-mobility">
                      <div class="knowledge-mobility-top">
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'mobility-solutions', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
<?php
                  if(have_rows("mobility_solutions_content")):
                    while(have_rows("mobility_solutions_content")): the_row();
?>

<?php
                    if(get_row_layout()=="image"):

                      $image = get_sub_field("mobility_image");
                      $image_url = $image['url'];
                      $image_alt = $image['alt'];
?>
                      <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="mobility-image">
                 
<?php
                    endif;
?>

<?php                    
                  endwhile;
                endif;
?>
<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?> 

 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'page', 
        'pagename' => 'knowledge-resources/mobility-solutions',
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                      <div class="knowledge-mobility-overlay">
                        <h3><?php the_title(); ?></h3>

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?> 

 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'mobility-solutions', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                        <h4><?php the_title(); ?></h4>
                      </div> <!-- .knowledge-mobility-overlay -->                      
                    </div> <!-- .knowledge-mobility-top -->                        

                    <div class="knowledge-mobility-intro">
                      <?php the_field("mobility_solutions_intro"); ?>
                      <a href="<?php the_permalink(); ?>" class="read-more">Read More</a>
                    </div> <!-- .knowledge-mobility-intro -->  
<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>

                    </div> <!-- .knowledge-mobility -->
                  </div> <!-- cols -->

                  <div class="large-4 large-pull-8 small-12 columns cols-2px">
                    <div class="knowledge-webinars">
                      <div class="knowledge-webinars-top">
                        <h3>Webinars</h3>
                        <span class="webinar-date"><?php the_field("webinar_date"); ?></span>
                        <span class="webinar-time"><?php the_field("webinar_time"); ?></span>
                        <span class="webinar-name"><?php the_field("webinar_name"); ?></span>
                        <a href="<?php the_field("webinar_link"); ?>" class="read-more"><?php the_field("webinar_link_text"); ?></a>
                      </div> <!-- .knowlege-webinars-top -->
                      <div class="knowledge-webinars-bot">
<?php
                        $image = get_field("speaker_image");
                        $image_url = $image['url'];
                        $image_alt = $image['alt'];
?>
                        <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>" class="webinar-speaker"> 

                        <div class="knowledge-webinar-speaker-info">
                          <h4>Featured Speaker</h4>
                          <span class="speaker-name"><?php the_field("speaker_name"); ?></span>
                          <span class="speaker-title"><?php the_field("speaker_title"); ?></span>
                        </div> <!-- .knowledge-webinar-speaker-info -->                       
                      </div> <!-- .knowledge-webinars-bot -->
                    </div> <!-- .knowledge-webinars -->
                  </div> <!-- cols -->
                </div> <!-- .row -->
              </div> <!-- .cols -->
            </div> <!-- .row -->
          </div> <!-- .cols -->                             
        </div> <!-- .row -->

        <div class="row">

          <div class="small-11 small-centered medium-12 large-12 columns">

            <div class="row">

              <div class="large-4 medium-12 columns">
                <div class="row">
                  <div class="small-12 columns cols-2px">
                    <div class="knowledge-engagements">
                      <div class="knowledge-engagements-top">
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'page', 
        'pagename' => 'knowledge-resources/speaking-engagements',
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                        <h3><?php the_title(); ?></h3>

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?> 
                      </div> <!-- .knowledge-engagements-top -->
                      <div class="knowledge-engagements-info">
 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        //'offset' => 0,
        'post_type' => 'speaking-engagements', 
        'orderby' => 'date',
        'order' => 'asc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);
?>
                      <div class="speaking-location-date"> 
                        <span class="speaking-location"><?php the_field("speaking_engagement_location"); ?></span> | <span class="speaking-date"><?php the_field("speaking_engagement_date"); ?></span>
                      </div> <!-- .speaking-location-date -->

                      <div class="speakers">
<?php
          if(have_rows("featured_speakers")):
            while(have_rows("featured_speakers")): the_row();
?>            
              <span class="speaker"><?php the_sub_field("speaker_name"); ?>, <?php the_sub_field("speaker_title"); ?></span>

             
<?php                        

            endwhile;
          endif;
?>                        
                      </div> <!-- .speakers -->
                      <h4><?php the_title(); ?></h4>

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>
                        <a href="<?php the_field("speaking_engagements_link"); ?>" class="read-more"><?php the_field("speaking_engagements_link_text"); ?></a>
                      </div> <!-- .knowledge-engagements-info -->                      
                    </div> <!-- .knowledge-engagements -->
                  </div> <!-- .small-12 -->
                  <div class="small-12 columns cols-2px">
                    <div class="knowledge-tax-guide">
                      <h3><?php the_field("tax_guide_title"); ?></h3>
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'page', 
        'pagename' => 'tax-services/annual-tax-guides',
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                      <div class="tax-image">
<?php
                    $image = get_field("tax_guide_image");
                    $image_url = $image['url'];
                    $image_alt = $image['alt'];
?>
                        <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">

                      </div> <!-- .tax-image -->
                      <div class="tax-description">
                          <?php the_field("tax_guide_description"); ?>

                         
                      </div> <!-- .tax-description -->
                        <div class="quantity-price">
                          <div class="quantity-price-label">                        
                              <?php the_field("quantity_price_label"); ?>
                          </div> <!-- .quantity-label -->                        
<?php                    

                              if(have_rows("quantity_pricing")):
?>
                          <div class="quantity-price-list">
<?php                                              
                                while(have_rows("quantity_pricing")): the_row();
?>
                            <span class="quantity-price-each">
                              <span><?php the_sub_field("quantity_price"); ?></span>
                              <span><?php the_sub_field("additional_information"); ?></span>
                            </span> <!-- .quantity -->                            
                            <span class="additional-information">
                              
                            </span> <!-- .additional-information -->                            
<?php
                                endwhile;
?>
                          </div> <!-- quantity-price-list -->
<?php                                              
                              endif;
?>
                        </div> <!-- .quantity-price -->                       

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>
                      <div class="tax-button">
<?php

                      if(get_field("tax_guide_button")):

?>

                  <a class="overview-form-button button-white" href="<?php the_field('tax_guide_button'); ?>" target="_blank"><?php the_field("tax_guide_button_text"); ?></a>
<?php
                      endif; 
?>
                       
                      </div> <!-- .tax-button --> 
                    </div> <!-- .knowledge-tax-guide -->
                  </div> <!-- .small-12 -->              
                </div> <!-- .row -->
              </div> <!-- cols -->

              <div class="large-8 medium-12 columns">
                <div class="row">
                  <div class="small-12 columns cols-2px">
                    <div class="knowledge-email">
                      <h3><?php the_field("email_heading"); ?></h3>
                      <p class="email-intro"><?php the_field("email_intro"); ?></p>
                      <?php the_field("email_shortcode"); ?>
                    </div> <!-- .knowledge-email -->
                  </div> <!-- .cols -->
                  <div class="small-12 columns">
                    <div class="row cols-2px">
                      <div class="small-12 medium-6 columns cols-2px">
                        <div class="knowledge-tax-updates">
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 2,
        //'offset' => 0,
        'post_type' => 'page',
        'pagename' => 'knowledge-resources/tax-updates', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>

                        <h3><?php the_title(); ?></h4>
                       

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>

<?php
      $posts = get_posts(array(
        'posts_per_page' => 2,
        //'offset' => 0,
        'post_type' => 'tax-updates', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);
            $post_id = get_the_ID();      
?>
                      <div class="tax-article">
                        <h4><?php the_title(); ?></h4>
                        <?php the_field("tax_update_intro"); ?>
                        <p><a href="/knowledge-resources/tax-updates#<?php echo $post_id; ?>" class="more-link">Read More</a></p>
                      </div> <!-- .tax-article -->

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>
                          <a href="<?php the_field("tax_updates_link"); ?>" class="read-more"><?php the_field("tax_updates_link_text"); ?></a>
                        </div> <!-- .knowledge-tax-updates -->
                      </div> <!-- cols -->
                      <div class="small-12 medium-6 columns cols-2px">
                        <div class="knowledge-subject-matters">
 <?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'page',
        'pagename' => 'knowledge-resources/subject-matters', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>

                        <h3><?php the_title(); ?></h4>
                       

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>

<?php
      $posts = get_posts(array(
        'posts_per_page' => 1,
        //'offset' => 0,
        'post_type' => 'subject-matters', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
                      <div class="subject-article">
                        <h4><?php the_title(); ?></h4>
                        <div class="author-info">
                          <span class="author-name"><?php the_field("author_name"); ?></span>
                          <span class="author-title"><?php the_field("author_title"); ?></span>
                         
                        </div> <!-- .author-info -->
                        <?php the_field("article_intro"); ?>
                        <p><a href="<?php the_permalink(); ?>" class="more-link">Read More</a></p>
                      </div> <!-- .subject-article -->

<?php
          wp_reset_postdata();
        endforeach;
      endif;       
?>

                        </div> <!-- .knowledge-subject-matters -->
                      </div> <!-- cols -->                  
                    </div> <!-- .row -->
                  </div> <!-- .cols -->
                </div> <!-- .row -->
              </div> <!-- .cols --> 
            </div> <!-- .row -->
          </div> <!-- .columns -->
        </div> <!-- .row -->

      </div> <!-- .knowledge-grid -->    


    </div> <!-- #main --> 


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
