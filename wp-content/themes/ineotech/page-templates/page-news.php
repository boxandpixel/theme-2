<?php
/**
 * Template Name: News
 *
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main" class="interior news">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>

    <div class="secondary-nav">

      <div id="service-nav-title">
        <span class="overview-page"><?php the_field('secondary_nav_category'); ?></span>
        <span class="overview-title"><?php the_field('secondary_nav_page'); ?></span>
      </div> <!-- #service-nav-title -->

    </div> <!-- .secondary-nav -->    
    
    <div id="main">
      <div class="page-title">
        <div class="row">
          <div class="small-11 small-centered columns">
            <h1><?php the_field("page_title"); ?></h1>
          </div> <!-- .columns -->
        </div> <!-- .row -->

      </div> <!-- .page-title -->
 <?php
      $posts = get_posts(array(
        'posts_per_page' => -1,
        //'offset' => 0,
        'post_type' => 'news', 
        'orderby' => 'date',
        'order' => 'desc'
      )); 
      

      if( $posts ):
        foreach( $posts as $post ):     
          setup_postdata($post);        
?>
      <div class="news-post">
        <div class="row">
            <div class="small-11 small-centered columns">

              <div class="row">
                <div class="small-12 large-10 end columns">
                  <h2><?php the_title(); ?></h2>
                  <time><?php the_time('F j, Y'); ?></time>
                </div> <!-- .columns -->
              </div> <!-- .row -->


<?php
        if(have_rows("news_post_options")):
          while(have_rows("news_post_options")): the_row();

            if(get_row_layout() == "image_left"):
?>              
              <div class="row">
                <div class="small-12 large-3 columns">
<?php
                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];

?>
                  <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                </div> <!-- .columns -->
                <div class="small-12 large-7 end columns">
                  <?php the_sub_field("visual_editor"); ?>
                </div> <!-- .columns -->                
              </div> <!-- .row -->
<?php
            elseif(get_row_layout() == "image_right"):
?>        
              <div class="row">

                <div class="small-12 large-3 large-push-9 columns">
<?php
                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];

?>
                  <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                </div> <!-- .columns --> 
                <div class="small-12 large-9 large-pull-3 columns">
                  <?php the_sub_field("visual_editor"); ?>
                </div> <!-- .columns -->                               
              </div> <!-- .row -->
<?php
            elseif(get_row_layout() == "image_top"):
?>
              <div class="row">
                <div class="small-12 columns">
<?php
                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];

?>
                  <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                </div> <!-- .columns -->
                <div class="small-12 columns">
                  <?php the_sub_field("visual_editor"); ?>
                </div> <!-- .columns -->                
              </div> <!-- .row -->
<?php
            elseif(get_row_layout() == "image_full_width"):
?>
              <div class="row">
                <div class="small-12 large-9 end columns">
<?php
                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];

?>
                  <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>">
                </div> <!-- .columns -->                 
                <div class="small-12 large-9 end columns">
                  <?php the_sub_field("visual_editor"); ?>
                </div> <!-- .columns -->
               
              </div> <!-- .row -->
<?php
            elseif(get_row_layout() == "image_blocks"):  
?>
              <div class="row">

                <div class="small-9 end columns">
                  <?php the_sub_field("visual_editor"); ?>

                  
<?php
                  if(have_rows("images")):
                    while(have_rows("images")): the_row();
?>
                   
                      <figure class="block-image">
<?php                        
                  $image = get_sub_field("image");
                  $image_url = $image['url'];
                  $image_alt = $image['alt'];

?>
                        <img src="<?php echo $image_url; ?>" alt="<?php echo $image_alt; ?>"> 
                        <figcaption><?php the_sub_field("caption"); ?></figcaption>                                               
                      </figure>
                    
<?php
                    endwhile;
                  endif;
?>                  

                </div> <!-- .small-12 -->

              </div> <!-- .row -->

<?php
            endif;
          endwhile;
        endif;
?>                
          </div> <!-- .small-12 -->
        </div> <!-- .row -->
      </div> <!-- .news-post -->

<?php
          wp_reset_postdata();
        endforeach;
      endif;
?>      


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
