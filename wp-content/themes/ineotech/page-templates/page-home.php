<?php
/**
 * Template Name: Home
 * 
 */

 get_header(); ?>

 <?php get_template_part( 'template-parts/featured-image' ); ?>

  <div id="page" role="main">

 <?php do_action( 'foundationpress_before_content' ); ?>
 <?php while ( have_posts() ) : the_post(); ?>
    
    <div class="home-scene js-home-fadeout" data-blackout="off">
      <div class="home-blur"></div> 
      <div class="home-lines"></div>

<?php 
              $intro_logo = get_field('intro_logo');
              $intro_logo_alt = $intro_logo['alt'];
              $intro_logo_url = $intro_logo['url'];
?>
            <img src="<?php echo $intro_logo_url; ?>" alt="Ineo Global Mobility" id="intro-logo" width="176">


<?php
          
              if(have_rows('intro_category_links')):
?>
            <ul id="intro-dots">
<?php                        
                while(have_rows('intro_category_links')): the_row();


                  $intro_category = get_sub_field('intro_category');
                  if($intro_category):
                    $post = $intro_category;
                    setup_postdata($post);
?>
              <li><?php the_title(); ?></li>

<?php
                    wp_reset_postdata();
                  endif;
                endwhile;
?>
            </ul> <!-- .dots-case -->
<?php             
              endif;
?> 

            <div class="home-intro">
              <div class="home-intro-column">
                <div class="home-intro-content">
                  <div class="row">
                    <div class="small-12 columns">
                  <?php the_field("intro_content"); ?>
                    </div> <!-- .small-12 -->
                  </div> <!-- .row -->
                </div> <!-- .home-intro-content -->
              </div> <!-- .home-intro-column -->
            </div> <!-- .home-intro -->
      
    </div> <!-- .home-scene -->

    <div id="ps-scroll-container" class="wrapper js-scrolling-content ps-container ps-theme-default ps-active-y">
      <div class="content-wrap home-content">

        <div class="mob-home-bg js-full-height" style="height: 736px;"></div>

        <div class="home-header-gap js-home-header-gap"></div>

        <div class="home-scene-content">
          <div class="home-lines js-home-fadeout">
            <!-- Start logo, dots and intro -->


            <!-- End Logo, dots, intro -->
          </div> <!-- .home-lines -->

          <div class="js-scroll-wrap intro-arrow" data-active="on">
              <span>Services &amp; Solutions</span>
              <a href="#" class="arrow-btn arrow-btn-down js-scroll-down" data-scroll-to-element="js-scroll-down-to" data-scroll-speed="1750">
               </a>
          </div> <!-- .intro-arrow -->

        </div> <!-- .home-scene-content -->          

      </div> <!-- .content-wrap -->

      <div class="standard-content-lockup js-scroll-down-to">
        <div class="row home-main"> 
          <div class="small-11 small-centered medium-10 medium-centered columns">
            <?php the_field('main_content'); ?>
          </div> <!-- .columns -->
        </div> <!-- .row -->  


        <div class="row home-categories">
<?php
            if(have_rows('main_category_links')):
              while(have_rows('main_category_links')): the_row();
?>

          <div class="small-12 medium-4 columns home-category">
 


<?php                                  
               if(get_row_layout() == "category_links_column"):
                  if(have_rows('category_links')):
                
?>
            <ul class="home-category-list">
<?php                        
                    while(have_rows('category_links')): the_row();
?>
                                        

<?php
                     
                      $page_link_heading = get_sub_field('page_link_heading');
                      $page_link = get_sub_field('page_link');
                  

                      if($page_link_heading):
                        $post = $page_link_heading;
                        setup_postdata($post);


?>
              <!-- row title here -->
              <li><h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2></li>

<?php
                        wp_reset_postdata();
                      endif; // if($page_link_heading)
?>

<?php  
                      if($page_link):
                      
                        $post = $page_link;
                        setup_postdata($post);
?>
              <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php
                        wp_reset_postdata();
                       
                      endif;   // if($page_link) 
?>

          
<?php                                                                 
                    endwhile; // while(have_rows('category_links'))
?>
            </ul>
<?php             
                  endif; // if(have_rows('cateogry_links'))

     
                endif; // if(get_row_layout() == "category_links_column"):
?>                
          </div> <!-- .columns -->

<?php                   

              endwhile; // while(have_rows('main_category_links')): the_row();
            endif; // if(have_rows('main_category_links')):
?> 
        </div> <!-- .row -->
        <div id="footer-container">
          <footer id="footer">
            <?php do_action( 'foundationpress_before_footer' ); ?>

            <div class="row">
              <div class="small-11 small-centered medium-10 medium-centered columns">
                <div class="row">
                  <div class="small-12 medium-2 columns">
<?php
            if(have_rows("footer_column_one", "options")):
              while(have_rows("footer_column_one", "options")): the_row();
                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];
?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>" width="150">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>

                  </div> <!-- .columns -->
                  <div class="small-12 medium-4 columns">
                    <div class="footer-column-2">
<?php
            if(have_rows("footer_column_two", "options")):

              while(have_rows("footer_column_two", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                    </div> <!-- .footer-column-2 -->
                  </div> <!-- .columns -->
                  <div class="small-12 medium-3 columns">
<?php
            if(have_rows("footer_column_three", "options")):

              while(have_rows("footer_column_three", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                  </div> <!-- .columns -->
                  <div class="small-12 medium-3 columns">
<?php
            if(have_rows("footer_column_four", "options")):

              while(have_rows("footer_column_four", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                  </div> <!-- .columns -->
                </div> <!-- .row -->
              </div> <!-- .columns -->
            </div> <!-- .row --> 

            <div class="row footer-bottom">
              <div class="small-11 small-centered medium-6 medium-centered columns">
                <p>Copyright <?php echo date('Y'); ?></p>
<?php 
              if(have_rows("footer_bottom", "options")):

                while(have_rows("footer_bottom", "options")): the_row();
                   
                  if(get_row_layout() == "page_links_section"):
                    if(have_rows("page_links", "options")):

?>
                <ul class="footer-page-links">
<?php                              
                      while(have_rows("page_links", "options")): the_row();
                  
                        $footer_links = get_sub_field("page_link", "options");
                        if($footer_links):
                          $post = $footer_links;
                          setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                          wp_reset_postdata();
                        endif;
                      endwhile;
?>
                </ul>
<?php                    
                    endif; //have_rows("page_links")
                  endif; // get_row_layout()
                endwhile; // have_rows("footer_bottom")
              endif; //have_rows(footer_bottom)
?>                
              </div> <!-- .columns -->
            </div> <!-- .row -->                                         
            <?php do_action( 'foundationpress_after_footer' ); ?>
          </footer>
        </div> <!-- .footer-container -->        

      </div> <!-- .standard-content-lockup --> 
  
    </div> <!-- #ps-scroll-container -->  

  


 <?php endwhile;?>

 <?php do_action( 'foundationpress_after_content' ); ?>


 </div> <!-- #page -->

 <?php get_footer();
