<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "container" div.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?> >
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<script src="https://use.typekit.net/lwt5ugh.js"></script>
		<script>try{Typekit.load({ async: true });}catch(e){}</script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); global $post;?> data-page="<?php echo $post->post_name; ?>">
	<?php do_action( 'foundationpress_after_body' ); ?>

	<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
	<div class="off-canvas-wrapper">
		<div class="off-canvas-wrapper-inner" data-off-canvas-wrapper>
		<?php get_template_part( 'template-parts/mobile-off-canvas' ); ?>
	<?php endif; ?>

	<?php do_action( 'foundationpress_layout_start' ); ?>

	<header id="masthead" class="site-header" role="banner">
<?php 
			$logo_header = get_field('header_logo', 'options');
			$logo_header_alt = $logo_header['alt'];
			$logo_header_url = $logo_header['url'];
?>			
		<div class="title-bar" data-responsive-toggle="site-navigation" style="width:100%" data-hide-for="xlarge">
			<button class="menu-icon float-right" type="button" data-toggle="mobile-menu"></button>
			<div class="title-bar-title <?php if(is_page('Home')){ echo 'title-bar-home';} ?>">

				<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img src="<?php echo $logo_header_url; ?>" alt="Ineo Logo"></a>
			</div>
		</div>

		<nav id="site-navigation" class="main-navigation top-bar" role="navigation">
			<div class="top-bar-left">
				<ul class="menu">
					
					<li class="home"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home" id="nav-logo"><img src="<?php echo $logo_header_url; ?>" alt="Ineo Logo"></a></li>
				</ul>
			</div>
			<div class="top-bar-right">
				<?php foundationpress_top_bar_r(); ?>

				<?php if ( ! get_theme_mod( 'wpt_mobile_menu_layout' ) || get_theme_mod( 'wpt_mobile_menu_layout' ) === 'topbar' ) : ?>
					<?php get_template_part( 'template-parts/mobile-top-bar' ); ?>
				<?php endif; ?>
			</div>
		</nav>
	</header>



	<section class="container">
		<?php do_action( 'foundationpress_after_header' );
