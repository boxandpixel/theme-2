<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "off-canvas-wrap" div and all content after.
 *
 * @package FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<?php 
	if(!is_page('Home')) { 
?>



		</section>
        <div id="footer-container">
          <footer id="footer">
            <?php do_action( 'foundationpress_before_footer' ); ?>

            <div class="row">
              <div class="small-11 small-centered medium-10 medium-centered columns">
                <div class="row">
                  <div class="small-12 medium-2 columns">
<?php
            if(have_rows("footer_column_one", "options")):
              while(have_rows("footer_column_one", "options")): the_row();
                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];
?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>" width="150">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>

                  </div> <!-- .columns -->
                  <div class="small-12 medium-4 columns">
                    <div class="footer-column-2">
<?php
            if(have_rows("footer_column_two", "options")):

              while(have_rows("footer_column_two", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                    </div> <!-- .footer-column-2 -->
                  </div> <!-- .columns -->
                  <div class="small-12 medium-3 columns">
<?php
            if(have_rows("footer_column_three", "options")):

              while(have_rows("footer_column_three", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                  </div> <!-- .columns -->
                  <div class="small-12 medium-3 columns">
<?php
            if(have_rows("footer_column_four", "options")):

              while(have_rows("footer_column_four", "options")): the_row();

                if(get_row_layout() == "image_section"):

                $footer_image = get_sub_field("image", "options");
                $footer_image_alt = $footer_image['alt'];
                $footer_image_url = $footer_image['url'];


?>
                <img src="<?php echo $footer_image_url; ?>" alt="<?php echo $footer_image_alt; ?>">                          
<?php 
                elseif(get_row_layout() == "visual_editor_section"):
?>
                <?php the_sub_field("visual_editor", "options"); ?>
<?php 
                elseif(get_row_layout() == "social_media_section"):

                  if(have_rows("social_media_icons", "options")):
?>
                <ul class="social-icons">
<?php                   
                    while(have_rows("social_media_icons", "options")): the_row();
?>                    
                  <li>
<?php                    
                    $social_image = get_sub_field("social_icon", "options");
                    $social_image_alt = $social_image['alt'];
                    $social_image_url = $social_image['url'];   


?>
                    <?php if($social_image): ?>                  
                    <a href="<?php the_sub_field('social_url'); ?>" target="_blank">                   
                      <img src="<?php echo $social_image_url; ?>" alt="<?php echo $social_image_alt; ?>" width="20">
                    </a>
                    <?php endif; ?>
                  </li>
<?php                  
                    endwhile;
?>                    
                </ul>
<?php                                 
                  endif;
?>                  

<?php
                elseif(get_row_layout() == "plain_text_section"):

?>
                <?php the_sub_field("plain_text", "options"); ?>
<?php 
                elseif(get_row_layout() == "page_links_section"):

                  if(have_rows("page_links", "options")):
?>
                <ul class="footer-page-links">
<?php                              
                    while(have_rows("page_links", "options")): the_row();
               
                      $footer_links = get_sub_field("page_link", "options");
                      if($footer_links):
                        $post = $footer_links;
                        setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                        wp_reset_postdata();
                      endif;
                    endwhile;
?>
                </ul>
<?php                    
                  endif;
                endif;
              endwhile;
            endif;
?>
                  </div> <!-- .columns -->
                </div> <!-- .row -->
              </div> <!-- .columns -->
            </div> <!-- .row --> 

            <div class="row footer-bottom">
              <div class="small-12 medium-6 medium-centered columns">
                <p>Copyright <?php echo date('Y'); ?></p>
<?php 
              if(have_rows("footer_bottom", "options")):

                while(have_rows("footer_bottom", "options")): the_row();
                   
                  if(get_row_layout() == "page_links_section"):
                    if(have_rows("page_links", "options")):

?>
                <ul class="footer-page-links">
<?php                              
                      while(have_rows("page_links", "options")): the_row();
                  
                        $footer_links = get_sub_field("page_link", "options");
                        if($footer_links):
                          $post = $footer_links;
                          setup_postdata($post);
?>
                        <li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>

<?php
                          wp_reset_postdata();
                        endif;
                      endwhile;
?>
                </ul>
<?php                    
                    endif; //have_rows("page_links")
                  endif; // get_row_layout()
                endwhile; // have_rows("footer_bottom")
              endif; //have_rows(footer_bottom)
?>                
              </div> <!-- .columns -->
            </div> <!-- .row -->                                         
            <?php do_action( 'foundationpress_after_footer' ); ?>
          </footer>
        </div> <!-- .footer-container -->   

		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>



<?php wp_footer(); ?>

<script>
  //var employee_num = ;
  $('.about-employees').flipster({
    itemContainer: 'ul',
    itemSelector: 'li',
    loop: 2,
    start: <?php the_field('employee_number', 'options'); ?>,
    style: 'infinite-carousel',
    spacing: 0,
    scrollwheel: false,
    nav: 'before',
    buttons: true
  });
</script>

<?php do_action( 'foundationpress_before_closing_body' ); ?>
</body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-28007030-1', 'auto');
  ga('send', 'pageview');
 
</script>
</html>

<?php 		
	} else {
?>		

		</section>


		<?php do_action( 'foundationpress_layout_end' ); ?>

<?php if ( get_theme_mod( 'wpt_mobile_menu_layout' ) === 'offcanvas' ) : ?>
		</div><!-- Close off-canvas wrapper inner -->
	</div><!-- Close off-canvas wrapper -->
</div><!-- Close off-canvas content wrapper -->
<?php endif; ?>



<?php wp_footer(); ?>



<?php do_action( 'foundationpress_before_closing_body' ); ?>


</body>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
 
  ga('create', 'UA-28007030-1', 'auto');
  ga('send', 'pageview');
 
</script>
</html>
<?php
	} 
?>
